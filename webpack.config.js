const HtmlWebPackPlugin = require("html-webpack-plugin");
module.exports = {
	devtool: "eval-source-map",
	entry: ["./src/index.js"],
	resolve: {
		modules: [
			'src',
			'node_modules',
		],
		extensions: ['*', '.js', '.jsx']
	},
	output: {
		path: __dirname + '/build',
		publicPath: '',
		filename: 'assets/[hash].js'
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ['babel-loader']
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
				exclude: '/node_modules/'
			},
			{
				test: /\.(png|jpg|gif|svg|jpeg|json)$/,
				use: [
					{
						loader: 'file-loader'
					}
				]
			}
		]
	},
	devServer: {
		contentBase: './build',
		hot: true,
		historyApiFallback: true,
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: 'public/index.html'
		})
	]
};