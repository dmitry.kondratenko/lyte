import React from 'react';
import ReactDOM from 'react-dom';
import Application from './component/Application.jsx';
import {BrowserRouter} from 'react-router-dom';
import EventStore from "./store/EventStore";
import {Provider} from "mobx-react";
import UserDataStore from "./store/UserDataStore";

const stores = {
	eventStore: new EventStore(),
	userDataStore: new UserDataStore()
};

ReactDOM.render(
	<BrowserRouter>
		<Provider {...stores}>
			<Application/>
		</Provider>
	</BrowserRouter>,
	document.getElementById('root')
);
