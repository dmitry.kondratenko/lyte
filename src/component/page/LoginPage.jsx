import React, {Component} from "react";
import Paper from "@material-ui/core/Paper";
import {withRouter} from "react-router";
import {TextField} from "@material-ui/core";
import {inject} from "mobx-react";
import Button from "@material-ui/core/Button";
import {withStyles} from "@material-ui/core/styles";
import {URL} from "../../util/url";
import {postUsersToken, register} from "../../util/fetch";
import {doIfEnter} from "../../util/keyCode";

@inject("userDataStore")
class LoginPage extends Component {

	constructor(props) {
		super(props);
		this.state = {
			email: "",
			password: "",
			loading: false,
			errors: {}
		}
	}

	onHomeButtonClick = () => {
		this.props.history.push(URL.ROUTER.HOME);
	};

	onLoginButtonClick = () => {
		const {
			email,
			password
		} = this.state;
		this.setState({loading: true});
		postUsersToken(email, password)
			.then((response) => {
				console.log(response);
				this.setState({errors: {}});
			})
			.catch((errors) => {
				this.setState({errors: errors || {}});
			})
			.finally(() => {
				this.setState({loading: false});
			})
	};

	onChange = ({target: {id, value}}) => {
		this.setState({[id]: value});
	};

	render() {
		const {
			classes
		} = this.props;
		const {
			loading,
			email,
			password,
			errors
		} = this.state;
		return (
			<div className={"full"}>
				<div className={"header"}>
					<Button
						variant={"outlined"}
						onClick={this.onHomeButtonClick}
					>
						Let me out, please!
					</Button>
				</div>
				<div
					className={"d-flex-center full hm"}
					onKeyDown={({keyCode}) => doIfEnter(keyCode, this.onLoginButtonClick)}
				>
					<Paper className={classes.paper}>
						<TextField
							error={!!errors.email}
							helperText={(errors.email || "") + ""}
							label={"E-mail"}
							id={"email"}
							value={email}
							onChange={this.onChange}
							className={classes.email}
						/>
						<TextField
							type={"password"}
							error={!!errors.password}
							helperText={(errors.password || "") + ""}
							label={"Password"}
							id={"password"}
							value={password}
							onChange={this.onChange}
							className={classes.password}
						/>
						<div>
							<Button
								disabled={loading}
								className={classes.button}
								onClick={this.onLoginButtonClick}
							>
								Login
							</Button>
						</div>
					</Paper>
				</div>
			</div>
		);
	}
}

export default withStyles(theme => ({
	paper: {
		padding: 20,
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
		width: 400,
		height: 400,
		borderRadius: 400
	},
	button: {
		margin: "10px 0",
	},
	email: {
		height: 50,
		width: 200,
		marginBottom: 20
	},
	password: {
		height: 50,
		width: 200,
		marginBottom: 20
	}
}))(withRouter(LoginPage));