import React, {Component} from "react";
import Paper from "@material-ui/core/Paper";
import {withRouter} from "react-router";
import EventList from "../EventList/EventList";
import Button from "@material-ui/core/Button";
import {URL} from "../../util/url";
import {withStyles} from "@material-ui/core/styles";

class HomePage extends Component {

	onSignUpClick = () => {
		this.props.history.push(URL.ROUTER.SIGN_UP);
	};

	onLoginClick = () => {
		this.props.history.push(URL.ROUTER.LOGIN);
	};

	render() {
		const {classes} = this.props;
		return (
			<div className={"full-width  page-container"}>
				<div className={"header"}>
					<Button
						className={classes.button}
						variant={"outlined"}
						onClick={this.onSignUpClick}
					>
						Sign Up
					</Button>
					<Button
						variant={"outlined"}
						onClick={this.onLoginClick}
					>
						Login
					</Button>
				</div>
				<div className={"d-flex-center full"}>
					<Paper>
						<EventList/>
					</Paper>
				</div>
			</div>
		);
	}
}

export default withStyles(theme => ({
	button: {
		margin: "0 20px"
	}
}))(withRouter(HomePage));