import React, {Component} from "react";
import {withStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import {Delete, Edit} from '@material-ui/icons';

class EditableList extends Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			newDocName: "",
			searchName: ""
		}
	}

	onChange = ({target: {id, value}}) => {
		this.setState({[id]: value});
	};


	render() {
		const {
			classes,
			onEdit,
			onDelete,
			loading,
			list
		} = this.props;
		const {
			searchName
		} = this.state;
		return (
			<div className={classes.wrapper}>
				<List className={classes.root}>
					{list.map((item, i) => {
						const {label, value} = item;
						return (
							<ListItem
								key={`document-item-${i}`}
								dense
								button
							>
								<ListItemText
									title={label}
									className={classes.listItem}
									primary={label}
								/>

								<ListItemSecondaryAction>
									<IconButton
										onClick={() => onEdit(value.id)}
										edge="end"
									>
										<Edit
											color={"action"}
										/>
									</IconButton>
									<IconButton
										onClick={() => onDelete(value.id)}
										disabled={loading}
										edge="end"
										aria-label="delete"
									>
										<Delete
											color={"action"}
										/>
									</IconButton>
								</ListItemSecondaryAction>
							</ListItem>
						);
					})}
				</List>
			</div>
		);
	}
}

export default withStyles(theme => ({
	root: {
		width: '100%',
		maxWidth: 360,
		height: 160,
		overflow: "auto",
		backgroundColor: theme.palette.background.paper,
	},
	icon: {
		margin: "10px",
		width: "25px",
		height: "25px"
	},
	wrapper: {
		width: 280
	},
	listItem: {
		overflow: "hidden",
		textOverflow: "ellipsis",
		display: "inline-block",
		marginRight: 60
	}
}))(EditableList);