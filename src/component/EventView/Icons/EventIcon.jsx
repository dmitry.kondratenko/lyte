import React from "react";
import {withStyles} from "@material-ui/styles";
import styles from "../../../style/eventIcon.jsx";
import ListItemIcon from "@material-ui/core/ListItemIcon";


const EventIcon = ({classes, ...other}) => {
	return (
		<ListItemIcon classes={classes}>
			<img
				{...other}
				className={"full"}
				alt={"Processing"}
			/>
		</ListItemIcon>
	);
};

export default withStyles(styles)(EventIcon);