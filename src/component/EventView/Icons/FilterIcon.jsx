import React from "react";
import png from "../../../../assests/filter.png";

const FilterIcon = ({...props}) => {
	return (
		<img
			src={png}
			{...props}
			alt={"Loading"}
		/>
	);
};

export default FilterIcon;