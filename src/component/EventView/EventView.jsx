import React, {Component} from "react";
import ListItem from "@material-ui/core/ListItem";
import {Typography, withStyles} from "@material-ui/core";
import ListItemText from "@material-ui/core/ListItemText";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";
import EventIcon from "./Icons/EventIcon";
import Paper from "@material-ui/core/Paper";
import {TEXT} from "../../util/text";
import Container from "@material-ui/core/Container";
import {formatDate} from "../../util/format";
import IconButton from "@material-ui/core/IconButton";
import {Edit} from "@material-ui/icons";

class EventView extends Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			opened: false
		}
	}

	onClick = () => {
		this.setState({opened: !this.state.opened});
	};

	renderRow = (header, text) => {
		const {classes} = this.props;
		return <Container>
			<Typography className={classes.rowHeader}>{header}</Typography>
			<Typography className={classes.rowValue}>{text}</Typography>
		</Container>
	};

	render() {
		const {
			opened
		} = this.state;
		const {
			event,
			classes
		} = this.props;
		const {
			category,
			organizer,
			ticketPriceCurrency,
			minTicketPrice,
			maxTicketPrice,
			uri
		} = event;
		return (
			<div className={classes.container}>
				<Paper className={classes.paper}>
					<ListItem button onClick={this.onClick}>
						<IconButton
							disabled={true}
							className={classes.iconButton}
							onClick={() => {}}
							edge="end"
						>
							<Edit/>
						</IconButton>
						<EventIcon src={event.logoUri}/>
						<ListItemText
							className={classes.eventText}
							primary={event.name}
						/>
						{opened ? <ExpandLess/> : <ExpandMore/>}
					</ListItem>
					<Collapse in={opened} timeout="auto" unmountOnExit>
						<Container>
							{this.renderRow(TEXT.NAME, event.name)}
							{
								!!event.startTime && !!event.finishTime
								&& this.renderRow(TEXT.TIME_DURATION, formatDate(event.startTime) + " - " + formatDate(event.finishTime))}
							{this.renderRow(TEXT.CATEGORY, category ? category.name : null)}
							{this.renderRow(TEXT.ORGANIZER, organizer ? organizer.name : null)}
							{this.renderRow(TEXT.TICKET_PRICE + ", " + ticketPriceCurrency, "From " + minTicketPrice + " to " + maxTicketPrice)}
							{this.renderRow(TEXT.EVENT_SITE, <a href={uri}>{uri}</a>)}
						</Container>
					</Collapse>
				</Paper>
			</div>
		);
	}
}

export default withStyles(theme => ({
	container: {
		width: 600
	},
	eventText: {
		maxWidth: 350,
		padding: "0 10px"
	},
	paper: {
		margin: 10,
		padding: 10
	},
	rowHeader: {
		fontWeight: "bold",
		fontSize: 18
	},
	rowValue: {
		fontSize: 14
	},
	iconButton: {
		marginRight: 10
	}
}))(EventView);