import React, {Component} from "react";
import TextField from '@material-ui/core/TextField';
import NumberFormat from 'react-number-format';
import {withStyles} from "@material-ui/core/styles";

const styles = {
	input: {
		height: 30,
		boxSizing: "border-box",
		padding: "0 5px",
		maxWidth: 150
	},
	root: {
		margin: 10,
		justifyContent: "center"
	}
};

const NumberInput = ({rootClasses, decimalScale, readOnly, classes, variant, suffix, prefix, title, min, max = 99999999, step, onChange, inputProps, ...props}) => {

	return (
		<TextField
			variant={variant || "standard"}
			inputProps={{min, max, step, suffix, prefix, readOnly, decimalScale, ...inputProps}}
			onChange={(event) => {
				if (typeof onChange !== "function") {
					return;
				}
				let number = Number(event.target.value);
				if (!isNaN(number)) {
					if (!isNaN(min) && number < min) {
						number = min;
					}
					if (!isNaN(max) && number > max) {
						number = max;
					}
				}
				onChange(number, event.target.id);
			}}
			InputProps={{
				inputComponent: NumberFormatCustom,
				classes: {input: classes.input}
			}}
			classes={{root: `${classes.root} ${rootClasses || ""}`}}
			{...props}
		/>
	)
};

class NumberFormatCustom extends Component {

	constructor(props, context) {
		super(props, context);
	}

	render() {
		const {format, inputRef, value, onChange, max, ...other} = this.props;

		return (
			<NumberFormat
				{...other}
				value={value}
				max={max || 999999999}
				allowLeadingZeros={false}
				defaultValue={0}
				getInputRef={inputRef}
				onValueChange={values => {
					const value = values.floatValue || 0;
					onChange({target: {value: value}});
				}}
				thousandSeparator
			/>
		);
	}
}

export default withStyles(styles)(NumberInput);