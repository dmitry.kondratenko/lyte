import React, {Component} from "react";
import List from '@material-ui/core/List';
import {withStyles} from "@material-ui/styles";
import EventView from "../EventView/EventView.jsx";
import FilterListIcon from '@material-ui/icons/FilterList';
import IconButton from '@material-ui/core/IconButton';
import {inject, observer} from "mobx-react";
import TablePagination from "@material-ui/core/TablePagination";
import CircularProgress from '@material-ui/core/CircularProgress';
import {Container, TextField} from "@material-ui/core";
import {Clear} from "@material-ui/icons";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import NumberInput from "../NumberInput/NumberInput";
import Paper from "@material-ui/core/Paper";

@inject("eventStore")
@observer
class EventList extends Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			filterOpened: false
		};
	}

	onFilterClick = () => {
		const {filterOpened} = this.state;
		const {eventStore} = this.props;
		if (filterOpened) {
			eventStore.setMinPrice(0);
			eventStore.setMaxPrice(0);
		}
		this.setState({filterOpened: !filterOpened});
	};

	onPageChange = (event, page) => {
		const {eventStore} = this.props;
		if (eventStore.loading) {
			return;
		}
		eventStore.changePage(page);
	};

	onClear = () => {
		this.props.eventStore.setFilterName("");
	};

	onSearchChange = ({target: {value}}) => {
		this.props.eventStore.setFilterName(value);
	};

	onMinPriceChange = (value) => {
		this.props.eventStore.setMinPrice(value);
	};

	onMaxPriceChange = (value) => {
		this.props.eventStore.setMaxPrice(value);
	};

	render() {
		const {
			filterOpened
		} = this.state;
		const {
			classes,
			eventStore
		} = this.props;
		const {
			limit,
			count,
			page,
			events,
			loading,
			filterName,
			minPrice,
			maxPrice
		} = eventStore;
		return (
			<Container>
				<Paper className={classes.header}>
					<Container className={classes.headerBox}>
						<IconButton
							onClick={this.onFilterClick}
							edge="end"
							title={"Filter"}
							className={classes.filterIcon}
						>
							<FilterListIcon/>
						</IconButton>
						<Box>
							<TextField
								id={"searchName"}
								onChange={this.onSearchChange}
								value={filterName}
								placeholder={"Search"}
							/>
							<IconButton
								onClick={this.onClear}
								edge="end"
								aria-label="add"
							>
								<Clear
									color={"action"}
								/>
							</IconButton>
						</Box>
					</Container>
					<Container className={classes.headerBox}>
						<Collapse in={filterOpened} timeout="auto" unmountOnExit>
							<NumberInput
								label={"Min price"}
								value={minPrice}
								onChange={this.onMinPriceChange}
							/>
							<NumberInput
								label={"Max price"}
								value={maxPrice}
								onChange={this.onMaxPriceChange}
							/>
						</Collapse>
					</Container>
				</Paper>
				<List
					component="nav"
					aria-labelledby="nested-list-subheader"
					className={classes.root}
				>
					{
						loading
							? <CircularProgress disableShrink/>
							: events.map((event, idx) =>
								<EventView
									event={event}
									key={idx}
									classNested={classes.nested}
								/>
							)
					}
				</List>
				<TablePagination
					component="nav"
					page={page}
					rowsPerPage={limit}
					count={count}
					onChangePage={this.onPageChange}
					rowsPerPageOptions={[]}
				/>
			</Container>
		);
	}
}

export default withStyles(theme => ({
	root: {
		width: 600,
		minHeight: 400,
		overflow: "auto",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "column"
	},
	nested: {
		paddingLeft: 10,
	},
	header: {
		display: "flex",
		justifyContent: "flex-start",
		alignItems: "center",
		margin: 10,
		padding: "10px 0 20px 0",
		flexDirection: "column"
	},
	filterIcon: {
		margin: "0 20px 0 10px"
	},
	headerBox: {
		width: "100%",
		display: "flex"
	},
	filterContainer: {
		display: "flex",
		margin: 20,
		justifyContent: "space-evenly"
	}
}))(EventList);