import React, {Component} from "react";
import "../app.css";
import {Route, Switch, withRouter} from "react-router";
import MainPage from "./page/HomePage";
import SignUpPage from "./page/SignUpPage";
import {URL} from "../util/url";
import LoginPage from "./page/LoginPage";

class Application extends Component {

	constructor(props, context) {
		super(props, context);
	}

	render() {
		return (
			<Switch>
				<Route exact path={URL.ROUTER.HOME} component={MainPage}/>
				<Route exact path={URL.ROUTER.SIGN_UP} component={SignUpPage}/>
				<Route exact path={URL.ROUTER.LOGIN} component={LoginPage}/>
			</Switch>
		);
	}
}

export default withRouter(Application);