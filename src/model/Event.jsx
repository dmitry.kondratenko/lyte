import {computed, observable} from "mobx";
import Organizer from "./Organizer";
import Category from "./Category";

class Event {

	@observable id = null;
	@observable provider = null;
	@observable name = null;
	@observable uri = null;
	@observable category = null;
	@observable organizer = null;
	@observable startTime = null;
	@observable finishTime = null;
	@observable ticketPriceCurrency = null;
	@observable minTicketPrice = null;
	@observable maxTicketPrice = null;
	@observable logoUri = null;

	store = null;

	constructor(store) {
		this.store = store;
	}

	@computed get asJson() {
		return {}
	}

	updateFromJson(
		{
			id,
			provider,
			name,
			uri,
			category,
			organizer,
			start_time,
			finish_time,
			ticket_price_currency,
			min_ticket_price,
			max_ticket_price,
			logo_uri
		}
	) {
		this.id = id;
		this.provider = provider;
		this.name = name;
		this.uri = uri;
		this.category = category ? new Category(this, category.id, category.name, category.provider) : null;
		this.organizer = organizer ? new Organizer(this, organizer.id, organizer.name, organizer.provider) : null;
		this.startTime = start_time;
		this.finishTime = finish_time;
		this.ticketPriceCurrency = ticket_price_currency;
		this.minTicketPrice = min_ticket_price;
		this.maxTicketPrice = max_ticket_price;
		this.logoUri = logo_uri;
	}
}

export default Event;