import {observable} from "mobx";

class Category {

	@observable id = null;
	@observable name = null;
	@observable provider = null;

	event = null;

	constructor(event, id, name, provider) {
		this.event = event;
		this.id = id;
		this.name = name;
		this.provider = provider;
	}
}

export default Category;