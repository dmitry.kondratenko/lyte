import {observable} from "mobx";

class Organizer {

	@observable id = null;
	@observable name = null;
	@observable uri = null;
	@observable logoUri = null;
	@observable provider = null;

	event = null;

	constructor(event, id, name, uri, logoUri, provider) {
		this.event = event;
		this.id = id;
		this.name = name;
		this.uri = uri;
		this.logoUri = logoUri;
		this.provider = provider;
	}
}

export default Organizer;