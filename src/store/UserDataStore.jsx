import {action, observable} from 'mobx';

class UserDataStore {

	@observable email = null;
	@observable authenticated = false;

	@action
	setEmail(email) {
		this.email =  email;
	}

	@action
	setAuthenticated(authenticated) {
		this.authenticated =  authenticated;
	}

}

export default UserDataStore;