import {action, observable, reaction} from 'mobx';
import {get, getEvents, getFilteredEvents} from "../util/fetch";
import Event from "../model/Event";

class EventStore {

	@observable events = [];
	@observable loading = false;
	@observable limit = 5;
	@observable count = 0;
	@observable page = 0;
	@observable filterName = "";
	@observable minPrice = 0;
	@observable maxPrice = 0;

	previousUrl = null;
	nextUrl = null;
	delay = 500;

	constructor() {
		this.events = [];
		this.getEvents();
		this.filterNameReaction = reaction(
			() => this.filterName,
			this.commonReaction,
			{delay: this.delay}
		);
		this.minPriceReaction = reaction(
			() => this.minPrice,
			this.commonReaction,
			{delay: this.delay}
		);
		this.maxPriceReaction = reaction(
			() => this.maxPrice,
			this.commonReaction,
			{delay: this.delay}
		);
	}

	commonReaction = () => {
		this.page = 0;
		this.getEvents();
	};

	getEvents = () => {
		this.loading = true;
		let promise;
		if (this.filterName || this.maxPrice || this.minPrice) {
			promise = getFilteredEvents(this.limit, this.filterName, this.minPrice, this.maxPrice);
		} else {
			promise = getEvents(this.limit);
		}
		promise
			.then(this.parseEvents)
			.finally(() => {
				this.loading = false;
			})
	};

	@action bindData = (e, name) => {
	};

	saveEvent(event) {
		const index = this.events.findIndex(item => item.id === event.id);
		if (index > -1) {
			this.events[index] = event;
		} else {
			this.events.push(event);
		}
	}

	@action
	setPage(page) {
		this.page = page;
	}

	@action
	changePage(page) {
		let url = null;
		if (this.page < page) {
			url = this.nextUrl;
		} else {
			url = this.previousUrl;
		}
		if (url) {
			this.loading = true;
			get(url)
				.then(this.parseEvents)
				.finally(() => {
					this.loading = false;
				})
		} else {
			this.events = [];
		}
		this.page = page;
	}

	@action
	setFilterName(filterName) {
		this.filterName = filterName;
	}

	@action
	setMinPrice(minPrice) {
		this.minPrice = minPrice;
	}

	@action
	setMaxPrice(maxPrice) {
		this.maxPrice = maxPrice;
	}

	parseEvents = ({results, count, previous, next}) => {
		this.events = results.map(item => {
			const event = new Event(this);
			event.updateFromJson(item);
			return event;
		});
		this.count = count;
		this.previousUrl = previous;
		this.nextUrl = next;
	}
}

export default EventStore;