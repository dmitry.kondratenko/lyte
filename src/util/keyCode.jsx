export const doIfEnter = (code, action) => {
    if (code === 13 && typeof action === "function") {
        action();
    }
};