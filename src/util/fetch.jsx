import {URL} from "./url";

export const register = (email, password) => {
	return post(URL.API_V1.USERS.REGISTER, {email, password});
};

export const postUsersToken = (email, password) => {
	return post(URL.API_V1.USERS.TOKEN, {email, password});
};

export const getEvents = (limit) => {
	return get(URL.API_V1.EVENTS.ROOT + `?limit=${limit}`);
};

export const getFilteredEvents = (limit, searchName, minPrice, maxPrice) => {
	return get(URL.API_V1.EVENTS.SEARCH + `?limit=${limit}&search=${searchName}&min_ticket_price=${minPrice || ""}&max_ticket_price=${maxPrice || ""}`);
};
// ---------------------------------------------------------- //
// ------------------------- COMMON ------------------------- //
// ---------------------------------------------------------- //


export const post = (url, body) => {
	return fetch(url, defaultOptions("POST", body))
		.then(processJsonResponse);
};

export const postNoResponse = (url, body) => {
	return fetch(url, defaultOptions("POST", body))
		.then(processEmptyResponse);
};

export const put = (url, body) => {
	return fetch(url, defaultOptions("PUT", body))
		.then(processJsonResponse);
};

export const get = (url) => {
	return fetch(url, defaultOptions("GET"))
		.then(processJsonResponse);
};

export const deleteById = (url) => {
	return fetch(url, defaultOptions("DELETE"))
		.then(processJsonResponse);
};

export const processEmptyResponse = (response) => {
	if (response.ok) {
		return response;
	} else {
		return response.text().then(text => {
			try {
				return Promise.reject(JSON.parse(text));
			} catch (err) {
				return Promise.reject({message: text, status: response.status});
			}
		});
	}
};

export const processJsonResponse = (response) => {
	if (response.ok) {
		return response.json();
	} else {
		return response.text().then(text => {
			try {
				return Promise.reject(JSON.parse(text));
			} catch (err) {
				return Promise.reject({message: text, status: response.status});
			}
		});
	}
};

const defaultOptions = (method, body) => {
	return {
		method,
		body: body ? JSON.stringify(body) : null,
		headers: {
			"Accept": "application/json;charset=UTF-8",
			"Content-Type": "application/json;charset=UTF-8",
		}
	}
};