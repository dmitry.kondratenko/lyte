export const TEXT = {
	PROVIDER: "Provider",
	NAME: "Name",
	URI: "Uri",
	CATEGORY: "Category",
	ORGANIZER: "Organizer",
	START_TIME: "Start time",
	TIME_DURATION: "Time duration",
	FINISH_TIME: "Finish time",
	TICKET_PRICE_CURRENCY: "Ticket price currency",
	MIN_TICKET_PRICE: "Min ticket price",
	MAX_TICKET_PRICE: "Max ticket price",
	TICKET_PRICE: "Ticket price",
	EVENT_SITE: "Site"
};