export const URL = {
	API_V1: {
		USERS: {
			ROOT: {
			},
			REGISTER: "http://api.my-events.site/api/v1/users/register",
			TOKEN: "http://api.my-events.site/api/v1/users/token"
		},
		EVENTS: {
			ROOT: "http://api.my-events.site/api/v1/events",
			SEARCH: "http://api.my-events.site/api/v1/events/search"
		}
	},
	ROUTER: {
		HOME: "/",
		SIGN_UP: "/signup",
		LOGIN: "/login"
	}
};