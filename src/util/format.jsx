import moment from "moment";
import {DATE_FORMAT} from "./ssttings";

export const formatDate = (date) => {
	return moment(date).format(DATE_FORMAT);
};